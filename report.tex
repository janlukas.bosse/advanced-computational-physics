%!TEX root = report.tex
% ----------------------------------------------------------------------------
% Use the KomaScript styling on A4 paper
% ----------------------------------------------------------------------------
\documentclass[a4paper,11pt,parskip=half,toc=bib]{scrartcl}
\usepackage[pass]{geometry}
\usepackage{todonotes}

% ----------------------------------------------------------------------------
% Use UTF-8 encoding
% ----------------------------------------------------------------------------
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\DeclareUnicodeCharacter{2212}{-}

% ----------------------------------------------------------------------------
% pretty fonts
% ----------------------------------------------------------------------------
%\usepackage{palatino}
\usepackage[sc]{mathpazo} % Use the Palatino font
\usepackage[T1]{fontenc} % Use 8-bit encoding that has 256 glyphs
\linespread{1.05} % Line spacing - Palatino needs more space between lines
\usepackage{microtype} % Slightly tweak font spacing for aesthetics

% ----------------------------------------------------------------------------
% Headings and spacing
% ----------------------------------------------------------------------------
\addtokomafont{disposition}{\rmfamily}
\usepackage[section]{placeins}
\makeatletter
\AtBeginDocument{%
  \expandafter\renewcommand\expandafter\subsection\expandafter{%
    \expandafter\@fb@secFB\subsection
  }%
}
\makeatother


% ----------------------------------------------------------------------------
% physics maths and code related things
% ----------------------------------------------------------------------------
\usepackage{listings}
\lstset{basicstyle=\ttfamily\footnotesize,breaklines=true}
\usepackage{physics}
\usepackage{amsmath, amsthm, amssymb, amsfonts}
\renewcommand{\vec}[1]{\boldsymbol{#1}}         % bold vectors


% ----------------------------------------------------------------------------
% Figures etc
% ----------------------------------------------------------------------------
\usepackage[export]{adjustbox}
\usepackage{wrapfig}
\usepackage[format=plain,indention=1.5em,small,
            labelfont=bf,up,textfont=it,up,skip=5pt]{caption}
\usepackage{subcaption}


% whack shit to get the pngs in the pgf to be included correctly
\usepackage{pgfplots}
\pgfplotsset{compat=1.16}
\usepackage{pgf}

% ----------------------------------------------------------------------------
% Citing and Referencing
% ----------------------------------------------------------------------------
\usepackage[backend=biber, style=phys, biblabel=brackets,
            citestyle=numeric-comp, doi=false, sorting=none]{biblatex}
\usepackage[autostyle]{csquotes}
\addbibresource{literature.bib}
\usepackage[pdfstartview=FitH,      % Oeffnen mit fit width
            breaklinks=true,        % Umbrueche in Links, nur bei pdflatex default
            bookmarksopen=true,     % aufgeklappte Bookmarks
            bookmarksnumbered=true, % Kapitelnummerierung in bookmarks
            colorlinks=true, urlcolor=blue, pdfborder={0 0 0},
            pdfencoding=auto
            ]{hyperref}
\usepackage{cleveref}
\usepackage{nameref}
\numberwithin{equation}{section}
\numberwithin{table}{section}
\numberwithin{figure}{section}


% ----------------------------------------------------------------------------
% Misc
% ----------------------------------------------------------------------------
\newcommand*{\ditto}{--- \raisebox{-0.5ex}{''} ---}


% ----------------------------------------------------------------------------
% The title page
% ----------------------------------------------------------------------------
\title{Project Report --- Monte Carlo Simulations for Particle Physics}
\author{Jan Lukas Bosse}
\date{\today}

\begin{document}

%\maketitle

\begin{titlepage}

\thispagestyle{empty}
\centering
\begin{minipage}[t]{.5\textwidth}
\includegraphics[height=1.5cm,left]{Logo_Uni_Goettingen}
\end{minipage}%
\begin{minipage}[t]{.5\textwidth}
\includegraphics[height=1.5cm,right]{Logo_Physik_links_eng}
\end{minipage}%

\vspace*{4.2cm}

\centering

\Large \textbf{Advanced Computational Physics Lab}

\vspace*{1cm}

%{\LARGE \bfseries
%  \titlefont{Computational Many-Body Methods} \\[0.5cm]}

{\LARGE \bfseries
  Project Report --- Monte Carlo Simulations for Particle Physics \\[1.5cm]}

\normalfont\normalsize
\begin{center}
        \textbf{\large Jan Lukas Bosse}\\[3mm]
\end{center}
\end{titlepage}

% ----------------------------------------------------------------------------
% And the main part
% ----------------------------------------------------------------------------

\newpage
%\rhead{}
\tableofcontents

\newpage
\setcounter{page}{1}

\section{Simulation of the \(e^+ e^- \to  q \overline{q}\)
         process at fixed-order QED}

In this first part we integrate the leading order matrix element for 
the \(e^+ e^- \to  q \overline{q}\) process using Monte Carlo integration. The
probability for this process to happen at a given scattering angle \(\theta\),
center of mass energy \(s\) and azimuthal angle \(\phi\) is proportional 
to the differential cross section
%
\begin{equation}
  f_{q\overline{q}}(s, \cos \theta, \phi) :=
  \frac{\dd{\sigma}_{q\overline{q}}}{\dd{s} \dd{\cos \theta} \dd{\phi}}
  = p(s) \frac{1}{8\pi} \frac{1}{4 \pi} \frac{1}{2s}
  |\mathcal{M}_{q\overline{q}}(s, \cos \theta, \phi)|^2,
\end{equation}
%
where \(p(s)\) is the distribution of the center of mass spectrum of the beam 
and \(\mathcal{M}_{q\overline{q}}\) is the leading order matrix element
as given in the exercise sheet. To find the total cross section
\(\sigma_{q\overline{q}}\) of the process
one has to integrate \(f_{q\overline{q}}\) over \(s, \theta\)
and \(\phi\). A closer look at \(\mathcal{M}_{q\overline{q}}\) shows that it 
is not dependent on \(\varphi\), so we can skip the \(\varphi\)-integration
and replace it by multiplying the end-result with \(2 \pi\). Furthermore,
\(\cos \theta\) is distributed uniformly when integrating in spherical coordinates,
while \(\theta\) is not. So we will integrate over \(\cos \theta\) from 
-1 to 1 instead of over \(\theta\). Besides \(s\) and \(\cos \theta\) the
integrand also depends on the quark flavour of the outgoing quarks. Since 
all outgoing flavours are equally likely and we integrate using Monte Carlo
integration, we can simply pick a random flavor for each sample to achieve the 
averaging over different quark flavors.

\begin{figure}[b]
  \centering
  \input{utils/integrand.pgf}
  \caption{The integrands \(f_{q \overline{q}}(s, \cos \theta, 0)\) for all
  possible outgoing quark flavours. Sadly, it is not easy to tell from this plot
  which of the quark flavours corresponds to which of the surfaces. But the
  general features are clear: Depending on the outgoing flavour \(q\) the
  integrand takes one of two  very similar forms. In either case
  \(f_{q\overline{q}}\) has its highes values along the \(s=M_Z^2\) line.}
  \label{fig:integrand}
\end{figure}


\subsection{Integrating \(\dd{\sigma}_{q\overline{q}}\) at fixed \(s\)}
\label{sec:1d_integration}
First, we calculate \(\sigma_{q\overline{q}}\) for \(p(s) = \delta(s - M_Z^2)\),
where \(M_Z\) is the mass of the Z-boson. Now the the \(s\)-dependence 
of the integrating also drops out and we only have to perform a 1-dimensional
integral. The code for the Monte Carlo integrator is found in
\texttt{integrator.MCIntegrator} and as a sampler we use a 
\texttt{integrator.UniformSampler} in the range
\((\cos \theta, y) \in [-1,1] \times [0, 30]\) with \(N_{try} = 100,000\)
samples. After multiplication with
\(2\pi\) for the \(\phi\) integration and other conversion factors we find that
%
\begin{equation}
  \sigma_{q\overline{q}}(s=M_Z^2) = 42510 \pm 200 \, \mathrm{pb}
\end{equation}
%
which agrees with the literature value given on the exercise sheet.

\subsection{Integrating over \(\cos \theta\) and \(s\)}
\label{sec:2d_uniform_integration}
Although the integrand has its largest values at the \(s=M_Z^2\) line, one
should also integrate over \(s\) to get the correct result for the 
cross section. So now we integrate over \(\cos \theta\) and \(s\), the latter 
in the interval \([(M_Z - 3 \Gamma_Z)^2, (M_Z + 3\Gamma_Z)^2]\). Since 
the integrand is fastly decaying in \(s\) this range should suffice to
approximate the integral for \(s \in [0, \infty]\).
We use mutatis mutandis the same integrator and sampler as in the previous part.
Because we now perform a 2D integral instead of a 1D integral we use now
\(N_{try} = 1,000,000\) samples. As a result we now obtain
%
\begin{equation}
  \sigma_{q\overline{q}} = 9950 \pm 100 \, \mathrm{pb}.
\end{equation}
%
Little surprisingly, this result is smaller than in the previous section. In 
the previous section we chose \(p(s) = \frac{1}{2} \delta(s-M_Z^2)\), which is
supported exactly along the ridge of highest values of the integrand,
while in this section we had
\(p(s) = N \chi_{[(M_Z - 3 \Gamma_Z)^2, (M_Z + 3\Gamma_Z)^2]}(s)\), where 
\(\chi_{[a,b]}(x)\) denotes the characteristic function on the interval \([a,b]\)
and \(N\) is a normalization constant.
Hence we sampled alot more smaller function values than in the previous section
and one expects the integral to be smaller.

\begin{figure}
  \centering
  \input{utils/histograms.pgf}
  \caption{Comparison of the acceptance probability as a function of \(s\)
    with the marginal integral
    \(\int f_{q\overline{q}}(s, \cos \theta, 0) \, \mathrm{d} \cos \theta\).
    For comparison
    we also show a Breit-Wigner distribution centenred at \(M_Z^2\) with scale
    \(M_Z^2 \Gamma_Z^2\). All three 
    distributions are normalized such that the interal over them is 1.
  }
  \label{fig:histograms}
\end{figure}

To check consistency with the 1D integration from the previous section one can
also compute the marginal 1D integrals
\(\int f_{q\overline{q}}(s, \cos \theta, 0) \, \mathrm{dcos} \theta\) for 
sufficiently closely spaced \(s\)-values and then compare their values with the
acceptance probabilities \(p_{acc}(s)\) of sample points as a function of their \(s\)-value 
in the 2D integration. This is done in \Cref{fig:histograms}.
For comparison we also show the Breit-Wigner distribution that we will 
use later for importance sampling. We see that both functions agree
qualitatively in their overall shape. But \(p_{acc}(s)\) is more peaked than 
the marginal integrals; More than could be explained by statistical fluctuations.
This hints, that there still must be some mistake in the code. See
\cref{sec:discussion} for more details. 


\subsection{Monte Carlo errors and number of samples}

One of the advantages of Monte Carlo integration over e.g. quadrature rules is 
that the error of the results scales as \(O\left(N_{try}^{-\frac{1}{2}}\right)\)
irrespective 
of the dimension of the integration domain while for said quadrature rule the 
neccesary number of function evaluations scales exponentially in the dimension
of the integration domain\footnote{I know, apples and oranges... Still, this is 
what people claim as the advntage of Monte Carlo integration, even though 
the true argument is a little more complicated than this.}.
We verify this claim by plotting the Monte Carlo error estimate as a function of
samples \(N_{try}\) \Cref{fig:monte_carlo_errors}. Little surprisingly, it
follows the \(\frac{1}{\sqrt{N}}\) prediction almost perfectly.

\begin{figure}[h]
  \centering
  \input{utils/monte_carlo_errors.pgf}
  \caption{Monte Carlo error estimate for the 1D integral in
    \cref{sec:1d_integration} (left) and for the 2D integral (right) as a function
    of samples \(N_try\). For better comparison we also plot a 
    \(\frac{1}{\sqrt{N_{try}}}\) line in both cases.}
  \label{fig:monte_carlo_errors}
\end{figure}


\subsection{Vegas integration}

One of the shortcomings of the naive Monte Carlo integration method used in 
\cref{sec:2d_uniform_integration} is that all \(x, y\) pairs get sampled with the 
same probability. This means that most of the samples in regions where the 
integrand is small get rejected thus decreasing the sampling efficiency and 
increasing the Monte Carlo error. One way around this is the Vegas algorithm
\cite{VegasAlgorithm}, which adapts its sampling grid based on where samples 
got previously accepted. We use the \texttt{vegas} python implementation
\cite{vegas_python} here with 10,000 samples split into 10 iterations.
The result is 
%
\begin{equation}
  \sigma_{q\overline{q}} = 9930 \pm 20 \, \mathrm{pb}.
\end{equation}
%
Again, this result agrees with the literature value given. More importantly,
the standard error is smaller than in \cref{sec:2d_uniform_integration}
despite us taking only one tenth of the samples.
This clearly shows the advantage of adapting the sampling algorithm to the integrand.
%
\begin{figure}[h]
  \centering
  \input{utils/vegas_grid.pgf}
  \caption{Sampling grid of the \texttt{vegas} algorithm after 10 iterations with
  1,000 samples. The orange line is along \(s=M_Z^2\), the region where 
  the integrand is largest and over which we integrated in
  \cref{sec:1d_integration}. Each rectangle in the grid has the same sampling 
  weight.}
  \label{fig:vegas_grid}
\end{figure}
%
\begin{lstlisting}[frame=none, float,
                   caption={Summary of the \texttt{vegas} algorithm after
                            10 iterations},
                   captionpos=b, label=lst:vegas_log, xleftmargin=.15\textwidth]
itn   integral        wgt average     chi2/dof        Q
-------------------------------------------------------
  1   0.0002562(40)   0.0002562(40)       0.00     1.00
  2   0.0002582(20)   0.0002578(18)       0.21     0.64
  3   0.0002573(17)   0.0002575(13)       0.13     0.88
  4   0.0002562(17)   0.0002570(10)       0.23     0.88
  5   0.0002564(15)   0.00025682(83)      0.20     0.94
  6   0.0002548(15)   0.00025635(73)      0.44     0.82
  7   0.0002560(16)   0.00025629(66)      0.37     0.90
  8   0.0002564(14)   0.00025631(60)      0.32     0.95
  9   0.0002560(15)   0.00025627(56)      0.28     0.97
 10   0.0002571(14)   0.00025639(52)      0.29     0.98
\end{lstlisting}

In \cref{fig:vegas_grid,lst:vegas_log} we show the Vegas integration grid 
and summary after the 10 iterations. One can clearly see, that the grid 
is denser around the maximal function values at the \(s=M_Z^2\) line. There 
is also some non-trivial binning in the \(\cos \theta\) direction, namely 
the values \(\cos \theta \approx \pm 1\) get sampled more often than
\(\cos \theta \approx 0\). Both make sense, when considering
\cref{fig:integrand}. In the first iteration, we get non-sensical \(Q\) and 
\(\chi^2\)-values of 1.0 and 0.0 respectively, but the value of the integral is
already close to the final value. In the subsequent iterations value of the
integral does not change by much anymore. This hints, that the integrand was 
"too simple" for vegas and it didn't need many samples to create an almost 
perfectly adapted grid.


\subsection{Importance sampling}

If one already has a priori knowledge about the rough shape of the integrand
one can use importance sampling instead of the Vegas algorithm to increase 
the sampling efficiency. The rough idea is to take more \(x\)-samples in the 
region where one knows the integrand to be large and restrict the \(y\)-sampling
range according to the value of the integrand  (plus some leeway to make sure 
that one could also obtain sample points above the integrand). The 
\(x\) and \(y\) samples have to be compatible that they fill the region around 
the integrand uniformly.

Since in our case the integrand is dominated by the Breit-Wigner distribution 
around \(s=M_Z^2\) we sample the \(s\) values from this distribution and 
the \(\cos \theta\) values again uniformly in \([-1, 1]\). Such a sampler 
is implemented in \texttt{integrator.BreitWignerSampler}. Running the importance
sampling algorithm with 10,000 samples (the same number as in
\cref{sec:2d_uniform_integration}) we obtain
%
\begin{equation}
  \sigma_{q\overline{q}} = 11310 \pm 50 \, \mathrm{pb}.
\end{equation}
%
Sadly, this is not the same result as in the previous parts. Even after lengthy
debugging sessions we couldn't find the reason why it isn't and eventually gave 
up. More details are in \cref{sec:discussion}. Nevertheless, it is important 
to note that the importance sampling algorithm increased the sampling 
efficiency significantly, see \cref{tab:comparison}, and that the Monte Carlo
error estimate is smaller.

\subsection{Comparison of Results}
\label{sef:comparison_of_results}

\begin{table}[htb]
\begin{tabular}{l|crrr}
                        & integral value                 & literature value              & efficiency  & samples \\ \hline
1D along \(s=M_Z^2\)    & $42510 \pm 200 \, \mathrm{pb}$ & $42250 \pm 50 \, \mathrm{pb}$ & 31 \%       & 10,000     \\
2D uniform sampling     & $9950 \pm 100 \, \mathrm{pb}$  & $9880 \pm 50 \,\mathrm{pb}$   & 9 \%        & 100,000    \\
2D importance sampling  & $11310 \pm 50 \, \mathrm{pb}$  & \ditto                        & 36 \%       & 100,000    \\
2D with \texttt{vegas}  & $9930 \pm 20 \, \mathrm{pb}$   & \ditto                        & N.A.        & 10,000    
\end{tabular}
\caption{Comparison of the integration results with the different algorithms.
The sampling efficiency is the fraction of accepted sample points out of all
sample points.}
\label{tab:comparison}
\end{table}

In \cref{tab:comparison} we show a summary of the results and sampling 
efficiencies obtained with the different results. As discussed previously, the
results for the 1D integration, the 2D integration with uniform sampling and the
\texttt{vegas} integration all agree with the literature values within the error
bars. Only the result obtained with importance sampling is significantly higher 
than the literature value, which hints that something must have gone wrong.
Still, the sampling efficiency of the importance sampling is four times higher 
than with uniform sampling and the Monte Carlo error estimate correspondingly 
only half as big. Interestingly, the Monte Carlo error estimate of the
\texttt{vegas}  algorithm is even smaller, even though we took only one tenth 
of the samples.

\section{Simulation of the parton cascade from the quark-antiquark pair at
all-order QCD}
In this second section we will simulate QCD showering and jet formation using 
the results from first section for the \(e^+ e^- \to q\overline{q}\) process 
as inputs. The goal is to compute quantities like the average number of particles
after showering and the differential jet rates, i.e. the distance scale at which 
\((n+1)\) jet events start to look like \(n\) jet events.

Or phrased more precisely: We got few hundred lines of undocumented python code 
and tried to get them to produce the results that were expected on the sheet. 
Our results are detailled below.

\subsection{The running coupling}

\begin{figure}[h]
  \centering
  \input{utils/running_coupling.pgf}
  \caption{The running coupling \(\alpha_s(t)\) for energy scales 
  \(1 \leq t \leq 10,000 ~\mathrm{GeV}^2\)}.
  \label{fig:vegas_grid}
\end{figure}

The coupling strength of QCD significantly depends on the energy scale of the 
interaction. This means that in this part \(\alpha\) has to be replaced by 
some \(\alpha_s(t)\). We plot this \(\alpha_s(t)\) and one can see that 
for large energy scales the coupling gets smaller, which means that at 
large energy scales perturbative methods will work well. For small energy scales,
on the other hand, the coupling diverges. This implies that at small energy 
scales perturbative methods will fail.


\subsection{Number of final particles after showering}

The class \texttt{shower.Shower} implements a simulation of QCD showering. As 
input it takes an "event" generated in the first section. Such an "event" is 
specified by the momenta, flavours and color charges of the ingoing 
electron-positron pair and the outgoing quark-antiquark pair.
If we generate 1000 such events, count the number of outgoing particles and
weight these numbers with the probability of the \(e^+e^- \to q\overline{q}\)
process, we can find the average number of final particles after showering.
This number also depends on the cutoff energy scale \(t_0\) at which 
one stops the showering algorithm.
With a cutoff energy scale of \(t_0 = 1.0 \, \mathrm{GeV}^2\) we find 
%
\begin{equation}
  \expval{n_{final}} \approx 6
\end{equation}
%
while a cutoff energy scale of \(t_0 = 1.5 \, \mathrm{GeV}^2\) produces
%
\begin{equation}
  \expval{n_{final}} \approx 5.2.
\end{equation}
%
The literature value is \(\expval{n_{final}} = 5.2 \pm 0.2\), but I assume at
the cutoff energy scale \(t_0 = 1.0 \, \mathrm{GeV}^2\). At least, this is the 
value that is hard coded as a default value in the code we were given.


\subsection{Jet clustering}

Because the spatial resolution of detectors is limited (and the number of final
particles is ill-defined anyways due to the diverging coupling) it is impossible
to observe all final particles. Instead particles similar trajectories get
bunched together in so-called "jets". In this part we investigate, at what 
detector resolution / distance scale a \(n+1\) jet event starts to look like 
a \(n\) jet event.

In \texttt{analysis.Algorithm} we implemented the Durham Jet algorithm
\cite{durham} which returns for a given event the list of distance scales
at which a \(n+1\) jet event starts to look like a \(n\) jet event. These lists 
of splitting scales can then be fed to a \texttt{analsis.Analysis} instance
which will create the according histograms which can in turn be plotted with 
\texttt{plot\_jet\_histograms}. The results are shown in
\cref{fig:differential_jet_resolutions}, where we can see that we can't see
anything.

\begin{figure}[ht]
  \centering
  \resizebox{\textwidth}{!}{\input{utils/differential_jet_resolutions.pgf}}
  \caption{"Comparison" of differntial jet resolutions between the data provided
  in \texttt{sherpa.yoda} and the data generated with our Clustering algorithm
  and the analysis code provided to us}.
  \label{fig:differential_jet_resolutions}
\end{figure}



\section{Discussion}
\label{sec:discussion}

The results in the first section look, with the exception of the importance
sampling, good. All values agree, within the error bars, with the values 
given in the literature. The scaling of the Monte Carlo error follows also
the expected \(\frac{1}{\sqrt{N}}\) law.

In the second part, on the other hand, the results do not agree with the 
literature values given in the sheet. To get the "correct" result
for the number of partons in the shower we had to
manually tweak the cutoff energy scale, which surely isn't the way one is 
supposed to arrive at the correct result. For the differential jet resolution
the situation looks even more dire. Our results don't even show up in the plot
when using \texttt{plot\_jet\_histogram()}. This is probably due to the 
\texttt{Runtime Warning: invalid value encountered in true\_divide} warning 
that \texttt{plot\_jet\_histogram()} throws when called with the data in 
\texttt{ours.yoda}. But a quick comparison of \texttt{sherpa.yoda} and
\texttt{ours.yoda} doesn't show anything suspicious. All the numbers are 
of roughly the same magnitude in both files. Given the amount of time we already
spent on this exercise and the fact that finding the bug he involves digging 
through code written by somebody else we decided to call this document a report.

\subsection{Importance sampling}
Sadly, our results for the importance sampling integration did not agree with 
the literature or the results from uniform sampling. One possible explanation
that we can not exclude but doesn't sound very likely is that this really is the 
correct value of the integral. Only with importance sampling 
could we integrate over the whole \(s\) domain from 
(unphysical) \(-\infty\) to \(\infty\), but with the uniform sampling and
\texttt{vegas} we had to restrict
\(s \in [(M_Z - 3 \Gamma_Z)^2, (M_Z + 3 \Gamma_Z)^2]\).
But since the integrand decays 
fastly\footnote{But it does not decay rapidly; That term is reserved to mean 
"faster than any rational function"} it doesn't sound plausible. To "prove" that
our importance sampling implementation is still correct (and the reason why we
eventually gave up trying to find a bug) we show in \cref{fig:3dplot} the 
integrand together with 1000 sampling points. The accepted sampling points 
are colored blue and the rejected samples are in red. One can see---up to the 
lack of a third dimension on this sheet of paper---that the samples appear to 
be Breit-Wigner distibuted in \(s\) and uniformly in \(\cos \theta\). 
Furthermore do the correct samples accepted respectively rejected. Lastly, we 
checked the algorithm by integrating a standard normal distribution with 
samples drawn from a Cauchy distribution. This also yielded the correct results 
and the corresponding code is found in the last few lines of
\texttt{exercise1.py}.

\begin{figure}
  \centering
  \input{utils/3dplot.pgf}
  \caption{Another plot of the integrand (in this case only for a down-type
  quark) together with 1000 samples from the Breit-Wigner distribution in \(s\)
  and uniform in \(\cos \theta\). The rejected samples, i.e. the points above
  the integrand, are show in red while the accepted samples, the points below 
  the integrand, are shown blue.}
  \label{fig:3dplot}
\end{figure}

\subsection{Some comments on the exercise}
While the first part of this exercise was well enough explained and our results 
look correspondingly good, the second part was much harder.
The code that we were given had neither docstrings nor typehints, so at first 
it often took some digging through the code what the arguments of the functions 
actually were. Take for example the \texttt{pdgid} argument of the
\texttt{Particle} class: It gets assigned to \texttt{self.pid}. This in turn 
then gets compared to \texttt{sf.flavs} in \texttt{shower.py:121}. So we know 
it must be of the same type as \texttt{sf.flavs}. But what type is it and which
values can it take? Well, \texttt{sf} is in the list \texttt{Shower.kernels}.
So let's have a look at the point where 
\texttt{Shower.kernels} gets initialized. Apparently there are three different 
kernels and from the names of the the kernel generating functions one can
now guess that 21 stands for "gluon" and 1-5 are the codes for quarks 
and -5--1 for anti-quarks. So \texttt{pdgid} seems to stand for pardigle-ID?
And what ID do electron and positron have? Apparently \(\pm 11\), or so I am
told that \texttt{pdgid} stands for "particle datagroup ID" and I can look up 
the codes there. But why make finding this information so hard? Why not simply 
write a docstring that has all the information? This frustration of having 
to search through the whole code to find out what exactly we are supposed to 
put in as arguments took most of the fun out of the exercise.

Additionally, for someone
without a particle-physics background it is very hard to debug the code and
find out what was wrong, since we had now idea what results would be sensible.
So we eventually decided to to hand in this report without properly solving 
the last exercise.


\printbibliography

\end{document}
