"""Numerical values of all constants. For their meaning refer to the exercise sheet"""
alpha = 1/129
alpha_M_Z = 0.118
M_Z = 91.2
Gamma_Z = 2.5
sin2_theta_W = 0.223
Q = {"e" : -1,
     "u" : 2/3, "c" : 2/3,
     "d" : -1/3, "s" : -1/3, "b" : -1/3,
    }
T3 = {"u" : 1/2, "c" : 1/2,
      "e" : -1/2, "d" : -1/2, "s" : -1/2, "b" : -1/2,
     }
N_q = 5
N_C = 3
f_conv = 3.89379656e8
flavours = ["e", "u", "c", "d", "s", "b", ]
out_flavours = ["u", "c", "d", "s", "b", ]
V = {f : T3[f] - 2 * Q[f] * sin2_theta_W for f in flavours}
A = {f : T3[f] for f in flavours}
kappa = 1 / (4 * sin2_theta_W * (1 - sin2_theta_W))
