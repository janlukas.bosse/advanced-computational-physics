import numpy as np
import matplotlib.pyplot as plt

def figsize(scale, ratio=(np.sqrt(5.0)-1.0)/2.0):
    r"""Figsizes that work in A4 LaTeX documents.

    Arguments
    ---------
    scale: Size of the plot. 1 Corresponds to filling the full \textwidth
    ratio: Aspect ratio  of the figure. Defaults to the golde ratio
    """
    fig_width_pt = 418.255           # Get this from LaTeX using \the\textwidth
    inches_per_pt = 1.0/72.27        # Convert pt to inch
    fig_width = fig_width_pt * inches_per_pt * scale    # width in inches
    fig_height = fig_width * ratio              # height in inches
    fig_size = (fig_width, fig_height)
    return fig_size



pgf_with_latex = {               # setup matplotlib to use latex for output
#    "pgf.texsystem": "pdflatex",   # change this if using xetex or lautex
#    "text.usetex": true,           # use LaTeX to write all text
    "font.family": "serif",
    "font.serif": [],              # blank entries should cause plots
                                    #to inherit fonts from the document
    "font.sans-serif" : [],
    "font.monospace" : [],
    "axes.labelsize" : 10,      # LaTeX default is 10pt font.
    "font.size" : 10,
    "legend.fontsize" : 8,     # Make the legend/label fonts a little smaller
    "xtick.labelsize" : 10,
    "ytick.labelsize" : 10,
#    "pgf.rcfonts" => false,
    "lines.linewidth" : 1.,
    "axes.linewidth" : 0.5,
    "axes.spines.right" : False,
    "axes.spines.top" : False,
    "axes.grid"           : False,
    "grid.linestyle" : "dotted",
    "grid.color"       :   "#808080",
    "legend.frameon" : True,

    "figure.figsize" : figsize(0.9),     # default fig size of 0.9 textwidth
#    "pgf.preamble" => [
#        r"\usepackage[utf8x]{inputenc}",   # use utf8 fonts
#        r"\usepackage[T1]{fontenc}",       # plot preamble
#        r"\usepackage{amssymb}",
#        ]
}

plt.matplotlib.rcParams.update(pgf_with_latex)