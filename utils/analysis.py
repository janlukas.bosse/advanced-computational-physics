from vector import Vec4
from particle import Particle
from histogram import Histo1D, Scatter2D

import math as m
from itertools import combinations
from typing import List


# Why the fuck turn this into a class? Simply to share self.ecm2??
class Algorithm:

    def Yij(self, p: Vec4, q: Vec4):
        pq = p.px * q.px + p.py * q.py + p.pz * q.pz
        return (2. * min(p.E, q.E)**2
                * (1.0 - min(max(pq / m.sqrt(p.P2() * q.P2()), -1.0), 1.0))
                / self.ecm2)

    def Cluster(self, event: List[Particle]):
        # TODO: implement the clustering algorithm here, and return a list of
        # splitting scales Yij, ordered from smallest Yij to largest Yij

        # NOTE: There is no fixed ycut, since we want to know at which point a
        # n-jet event starts looking like a (n+1)-jet event (compare the kT jet
        # fraction plot in the lecture, which is basically an integrated
        # version of what we'd like plot). Instead, keep clustering until only
        # two jets are left.

        self.ecm2 = sum(e.mom for e in event).M2()
        ys = []
        while (N := len(event)) > 2:
            yij = {}
            for i, j in combinations(range(N), 2):
                yij[(i, j)] = self.Yij(event[i].mom, event[j].mom)
            
            # find the minimum distance among the remaining particles
            # and corresponding particles
            dist = 1000000000.
            i, j = 0, 0
            for p1, p2 in combinations(range(N), 2):
                if (newdist := yij[(p1, p2)]) < dist:
                    dist = newdist
                    i, j = p1, p2

            # and combine them:
            event[i].mom += event[j].mom
            event.pop(j)
            self.ecm2 = sum(e.mom for e in event).M2()
            ys.append(dist)
                
        return ys


class Analysis:

    def __init__(self, algorithm):
        self.n = 0.
        self.ynm = [Histo1D(100, -4.3, -0.3,
                            '/LL_JetRates/log10_y_{0}{1}'.format(i + 2, i + 3))
                    for i in range(4)]
        self.ynm_integ = [Scatter2D(
            100, -4.3, -0.3, '/LL_JetRates/integ_log10_y_{0}'.format(i + 2))
            for i in range(5)]
        self.duralg = algorithm

    def Analyze(self, event, w):
        self.n += 1.

        # fill differential j -> (j+1) splitting scale distributions
        kt2 = self.duralg.Cluster(event)
        for j in range(len(self.ynm)):
            self.ynm[j].Fill(m.log10(kt2[-1 - j]) if len(kt2) > j else -5., w)

        # fill integrated j-jet rates
        previous_logy = 1e20
        for j in range(len(self.ynm_integ) - 1):
            s = self.ynm_integ[j]
            logy = m.log10(kt2[-1 - j]) if len(kt2) > j else -5.0
            for p in s.points:
                if p.x > logy and p.x < previous_logy:
                    p.y += w
            previous_logy = logy
        for p in self.ynm_integ[-1].points:
            if p.x < previous_logy:
                p.y += w

    def Finalize(self, name):
        for h in self.ynm:
            h.ScaleW(1. / self.n)
        for s in self.ynm_integ:
            s.ScaleY(1. / self.n)
        file = open(name + ".yoda", "w")
        file.write("\n\n".join([str(h) for h in self.ynm]))
        file.write("\n\n")
        file.write("\n\n".join([str(s) for s in self.ynm_integ]))
        file.close()
