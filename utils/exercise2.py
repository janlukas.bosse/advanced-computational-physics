#!/usr/bin/env python

# In[1]:

from integrator import *
from crosssection import *
from plotsettings import *

import numpy as np 
import matplotlib.pyplot as plt
import uncertainties.unumpy as unp
import vegas


# In[2]:


from alphas import *
from particle import *
from shower import *
from vector import *
from analysis import *
from yoda import *


# In[3]:


alpha_s = AlphaS(const.M_Z, const.alpha_M_Z)


# In[4]:


ts = np.logspace(0, 4)
couplings = np.empty_like(ts)
for (i, t) in enumerate(ts):
    couplings[i] = alpha_s(t)


# In[5]:


fig, ax = plt.subplots(figsize=figsize(0.7))
ax.plot(ts, couplings)
ax.set_xscale("log")
ax.set_xlabel(r"$t~[\mathrm{GeV}^2]$")
ax.set_ylabel(r"$\alpha_s(t)$")

fig.tight_layout()
fig.savefig("running_coupling.pgf")


# In[6]:


def f(x, q):
    "The weight function"
    mel = eeqq_matrix_element(x[0], x[1], x[2], q)
    return mel / x[0]


# In[7]:


def sampler():
    "Returns some uniformly distributed ee->qq events together with their weight"
    s = np.random.uniform((const.M_Z - 3*const.Gamma_Z)**2,
                          (const.M_Z + 3*const.Gamma_Z)**2)
    costheta = np.random.uniform(-1, 1)
    phi = np.random.uniform(0, 2 * np.pi)
    pid = np.random.randint(1, 5)
    color = np.random.randint(1, 3)
    
    x = np.array([s, costheta, phi]) 
    weight = f(x, const.out_flavours[pid])
    return np.array([s, costheta, phi]), pid, color, weight


# In[8]:


# with uniformly sampled events and weights
def runshower(t0=1.0):
    """"Create a ee -> qq event distributed according to the results of 1
    and run the shower algorithm on it.
    
    Returns
    -------
    Tuple: ([eminus, eplus, q1, q2, ....], weight) A list of outgoing particles
           and the weight of the event
    """
    x, pid, color, weight = sampler()
    s, costheta, phi = x

    E = 0.5 * np.sqrt(s)
    sintheta = np.sign(costheta) * np.sqrt(1-costheta**2)
    sinphi = np.sin(phi)
    cosphi = np.cos(phi)

    p1 = E * Vec4(1, 0, 0, 1)
    p2 = E * Vec4(1, 0, 0, -1)
    q1 = E * Vec4(1, -cosphi * sintheta, -sinphi * sintheta , -costheta)
    q2 = E * Vec4(1, cosphi * sintheta, sinphi * sintheta , costheta)

    eminus = Particle(11, p1)
    eplus = Particle(-11, p2)
    quark = Particle(pid, q1, [color, 0])
    antiquark = Particle(-pid, q2, [0, color])

    event = [eminus, eplus, quark, antiquark]
    shower = Shower(alpha_s, t0=t0)
    shower.Run(event, const.M_Z**2)
    return event, weight


# In[9]:


n_particles = np.empty(1000, dtype=np.uint8)
weights = np.empty(1000)
for i in range(1000):
    event, weight = runshower(1.5)
    n_particles[i] = len(event)
    weights[i] = weight


# In[10]:


# subtract 2 for the electron-positron that are still in the list
n_particles_expval = np.average(n_particles, weights=weights) - 2 
print(f"average number of partons in the shower: {n_particles_expval}")


# ## d)

# In[23]:


algorithm = Algorithm()
analysis = Analysis(algorithm)
for _ in range(10000):
    event, weight = runshower()
    analysis.Analyze(event[2:], weight)
    #analysis.Analyze(event, weight)
analysis.Finalize("ours")


# In[24]:


fig, ax = plot_jet_histograms(["../sherpa.yoda", "ours.yoda"])
fig.tight_layout()
fig.savefig("differential_jet_resolutions.pgf")

