import constants as const
from typing import Callable, List, Tuple
from uncertainties import ufloat
import uncertainties.unumpy as unp
import numpy as np
from histogram import Histo1D


class UniformSampler(object):
    def __init__(self, domain, codomain):
        """A sampler for a rectangular sampling domain
        
        Arguments
        ---------
        domain:   The sampling domain as a list of tuples [(x0, x1), (y0, y1), ...]
        codomain: The sampling codomain as a tuple. Should satisfy 
                  codomain[0] < min(func), max(func) < codomain[1]
        """
        self.domain = domain
        self.domain_volume = 1
        self.codomain = codomain
        for varrange in domain:
            self.domain_volume *= varrange[1] - varrange[0]
        self.volume = self.domain_volume * (codomain[1] - codomain[0])
            
    def __call__(self):
        """Gives a sample (x, y) uniformly distributed in `domain` x `codomain`"""
        x = np.random.uniform(size=(len(self.domain)))
        for (dim, dom) in enumerate(self.domain):
            x[dim] = dom[0] + x[dim] * (dom[1] - dom[0])
        y = np.random.uniform(self.codomain[0], self.codomain[1])
        return x, y


class BreitWignerSampler(object):
    def __init__(self, prefactor):
        """A sampler for the Breit-Wigner Distribution.
        
        It returns samples `((s, costheta), y)` where `s` is 
        sampled from the Breit-Wigner distribution, `costheta` uniformly
        and  `0 < y < prefactor * breit_wigner(s)`. 
        
        Arguments
        ---------
        prefactor: The 'height' of the distribution. Should be chose, as low
                   as possible, that the integrand isn't higher than highest 
                   samples this returns.
        """
        self.prefactor = prefactor
        self.volume = 2 * prefactor
        self.codomain = (0, prefactor / (np.pi * const.M_Z * const.Gamma_Z))
        self.domain_volume = 2
        
    def __call__(self):
        """
        Gives sample ([s, costheta], y) uniformly distributed below the 
        `prefactor/((s-M_Z**2)**2  + Gamma_Z**2 * M_Z**2)` curve.
        """
        s = np.random.uniform()
        s = const.Gamma_Z * const.M_Z * np.tan(np.pi * (s - 0.5)) + const.M_Z**2
        costheta = np.random.uniform(-1,1)
        y = np.random.uniform(0, self.prefactor * const.Gamma_Z * const.M_Z
            / (np.pi * ((s - const.M_Z**2)**2 + const.M_Z**2 * const.Gamma_Z**2)))
        return np.array([s, costheta]), y


class BreitWignerSampler3D(object):
    def __init__(self, prefactor):
        """A sampler for the Breit-Wigner Distribution.
        
        It returns samples `((s, costheta, phi), y)` where `s` is 
        sampled from the Breit-Wigner distribution, `costheta` and `phi`
        uniformly and  `0 < y < prefactor * breit_wigner(s)`. 
        
        Arguments
        ---------
        prefactor: The 'height' of the distribution. Should be chose, as low
                   as possible, that the integrand isn't higher than highest 
                   samples this returns.
        """
        self.prefactor = prefactor
        self.volume = 4 * np.pi * prefactor
        self.codomain = (0, prefactor / (np.pi * const.M_Z * const.Gamma_Z))
        self.domain_volume = 2
        
    def __call__(self):
        """See self.__init__()"""
        s = np.random.uniform()
        s = const.Gamma_Z * const.M_Z * np.tan(np.pi * (s - 0.5)) + const.M_Z**2
        costheta = np.random.uniform(-1,1)
        phi = np.random.uniform(0, 2*np.pi)
        pid = np.random.randint(1, 5)
        color = np.random.randint(1, 3)
        y = np.random.uniform(0, self.prefactor * const.Gamma_Z * const.M_Z
            / (np.pi * ((s - const.M_Z**2)**2 + const.M_Z**2 * const.Gamma_Z**2)))
        return np.array([s, costheta, phi]), pid, color, y


class CauchySampler(object):
    def __init__(self, prefactor):
        """
        Same as the BreitWignerSampler, but with a standard Cauchy-Distribution.
        """
        self.prefactor = prefactor
        self.volume = prefactor
        self.codomain = (0, prefactor)
        self.domain_volume = 1
        
    def __call__(self):
        x = np.random.uniform()
        x = np.tan(np.pi * (x - 0.5)) 
        y = np.random.uniform(0, self.prefactor / (np.pi * (x**2 + 1)))
        return x, y


class MCIntegrator(object):
    def __init__(self, func: Callable, sampler,
                 histograms: bool =False, histbins=20):
        """A Monte-Carlo integrator.
    
        Arguments:
        ----------
        func :        The function, that we want to integrate.
        sampler :     A sampling function that returns (x, y) values lying
                      around the integration region.
        histograms :  Record histograms of the accepted (x, y)?
        histbins :    Number of bins in the histograms. Has no effect, if
                      `histograms=False`
        """
        self.func = func
        self.sampler = sampler
        self.N_acc = 0 
        self.N_try = 0
        self.I = ufloat(0, 0)
        
        self.histograms = []
        if histograms:
            for (i, varrange) in enumerate(self.sampler.domain):
                self.histograms.append(
                    Histo1D(histbins, varrange[0], varrange[1], name=f"x{i}"))
    
    @classmethod
    def uniform(cls, func: Callable, domain: List[Tuple], codomain: Tuple,
                *args, **kwargs):
        "A Monte-Carlo integrator on a rectangular domain. (<=> uniform sampling)."
        sampler = UniformSampler(domain, codomain)
        return cls(func, sampler, *args, **kwargs)
    
    def __call__(self, N_try):
        "Integrate using N_try samples and return the estimate of the integral"
        for _ in range(N_try):
            x, y = self.sampler()
            if (f := self.func(x)) > y:
                self.N_acc += 1
                if self.histograms:
                    for (i, hist) in enumerate(self.histograms):
                        hist.Fill(x[i], f)
                        
        self.N_try += N_try
        p_succ = self.N_acc / self.N_try
        self.I = ufloat(self.sampler.volume * p_succ,
            self.sampler.volume * np.sqrt(p_succ * (1 - p_succ) / self.N_try)               
        )
        self.I += self.sampler.domain_volume * self.sampler.codomain[0]
        return self.I
    
    def __repr__(self):
        return str(self.I)
    
    def reset(self):
        "Set N_try and N_acc to zero again"
        self.N_acc = self.N_try = 0
        self.I = ufloat(0, 0)
        
    def samples(self, N_samples):
        "Get some samples of the function for debugging purposes"
        samples = np.empty(N_samples)
        for i in range(N_samples):
            x = self.sampler()
            samples[i] = self.func(x) 
        return samples