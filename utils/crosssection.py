import constants as const

import numpy as np


def chi1(s):
    "The chi1 function from the sheet"
    out = const.kappa * s * (s - const.M_Z**2)
    out /= (s - const.M_Z**2)**2 + const.Gamma_Z**2 * const.M_Z**2
    return out


def chi2(s):
    "The chi2 function from the sheet"
    out = const.kappa**2 * s**2
    out /= (s - const.M_Z**2)**2 + const.Gamma_Z**2 * const.M_Z**2
    return out


def breit_wigner_pdf(s):
    p = const.Gamma_Z * const.M_Z
    p /= np.pi * ((s - const.M_Z**2)**2 + const.M_Z**2 * const.Gamma_Z**2)
    return p


def eeqq_matrix_element(s : float, costheta : float, phi : float, q):
    """The squared ee -> qq matrix elemenent.
    
    Arguments:
    ----------
    s :        The centre of mass energy
    costheta : The angle between the incoming e+ and the outgoing q
    phi :      The azimuthal angle of outgoing quark in the plane perpendicular
               to the beam
    q :        one of ["u", "c", "d", "s", "b"], the flavour of the outgoing
               quarks
    """
    prefactor = (4 * np.pi * const.alpha)**2 * const.N_C
    term1 = 1 + costheta**2
    term1 *= (const.Q["e"]**2 * const.Q[q]**2
              + 2 * const.Q["e"] * const.Q[q] * const.V["e"] * const.V[q]
                  * chi1(s)
              + (const.A["e"]**2 + const.V["e"]**2)
                 * (const.A[q]**2 + const.V[q]**2)
                 * chi2(s)
             )
    term2 = np.copy(costheta)
    term2 *= (4. * const.Q["e"] * const.Q[q] * const.A["e"] * const.A[q]
                 * chi1(s)
              + 8. * const.A["e"] * const.V["e"] * const.A[q] * const.V[q]
                   * chi2(s)
             ) 
    return prefactor * (term1 + term2)


def eeqq_mel_random_q(s, costheta, phi):
    "Wrapper around `eeqq_matrix_element` that picks a random flavour"
    q = np.random.choice(const.out_flavours)
    return eeqq_matrix_element(s, costheta, phi, q)